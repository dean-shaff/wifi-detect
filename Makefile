CC=g++
PREFIX=/usr/local
DEBUG=0

all: wifi-detect.o

.PHONY: wifi-detect.o install

wifi-detect.o: wifi-detect.cpp
	$(CC) $^ -o $@ -lblkid -DDEBUG=$(DEBUG)

install: wifi-detect.o
	install $^ $(PREFIX)/bin/wifi-detect
