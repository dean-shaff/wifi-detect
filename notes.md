## Notes

From [here](https://unix.stackexchange.com/questions/418132/how-to-make-a-script-act-on-a-usb-drive-when-it-is-mounted):

In `/etc/udev/rules.d/10-local.rules`:

KERNEL=="sd?", SUBSYSTEM=="block", ATTR{removable}=="1", RUN+="/path/to/script.sh"

Okay, it looks like we also have to reload the rules. I think we do this by:

```
sudo udevadm control --reload && sudo udevadm trigger
```

I can figure out at which /dev/sd* my flash drive is mounted at with

```
mount | grep sd
```

I can figure out what my device name is with `lsblk`

I can monitor udev events with

```
udevadm monitor
```

I'm following [this](https://opensource.com/article/18/11/udev) tutorial

I've created a file /usr/local/bin/trigger.sh with the following contents:

```
#!/usr/bin/bash

/usr/bin/date >> /tmp/udev.log
```


The following will not work:

KERNEL=="sd?", SUBSYSTEM=="block", ATTR{removable}=="1", RUN+="$HOME/personal/wifi-detect/script.sh"

I think this is because the script is not in the default PATH. we have to put it in some place like `/usr/local/bin`.

I'm seeing if I can pass parameters to the trigger script:


'%E{ID_FS_LABEL}' '%E{DEVNAME}'


get partition info:

https://stackoverflow.com/questions/8663643/linux-c-programming-how-to-get-a-devices-partition-infomation
