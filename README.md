## Auto Wifi Detection

Say you have some headless device, like a raspberry pi that you transport around a lot. It would be nice if you didn't have to either connect it to an ethernet port or a keyboard/mouse/monitor in order to connect to a novel wifi network. `wifi-detect` to the rescue! Once installed on your device, `wifi-detect` allows you to plug in a USB stick with the wifi details and automatically connect to the wifi network.

### Installation

First, install the libblkid headers. On Ubuntu:

```
sudo apt install libblkid-dev
```

Now we have to modify our udev rules. Add the following line to `/etc/udev/rules.d/80-local.rules` (need to be root):

```
KERNEL=="sd?", SUBSYSTEM=="block", ATTRS{removable}=="1", ACTION=="add", RUN+="/usr/local/bin/wifi-detect '%E{DEVNAME}'"
```

Either restart, or reload udev rules:

```
sudo udevadm control -R
```

Now, build and install the wifi-detect executable:

```
sudo make install
```

### Usage

On your USB stick, create a file `wifi.txt` in the root directory with the wifi SSID and password stored as follows:

```
SSID
password
```

Right now, the `wifi-detect` executable is not very sophisticated, so the SSID has to be the first line, and the password has to be the second line. No comment lines allowed.
