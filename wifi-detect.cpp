#include <cstring>
#include <stdlib.h>
#include <cctype>
#include <map>
#include <tuple>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <iostream>
#include <dirent.h>
#include <errno.h>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <mntent.h>
#include <blkid/blkid.h>

#ifndef DEBUG
#define DEBUG 1
#endif

const std::string log_file_path = "/tmp/wifi-detect.log";

void get_mnt_tbl (std::map<std::string, std::string>& tbl)
{
  struct mntent *ent;
  FILE *aFile;

  aFile = setmntent("/etc/mtab", "r");
  if (aFile == NULL) {
    perror("setmntent");
    // logfile << "wifi-detect: setmntent error" << std::endl;
    exit(1);
  }
  while (NULL != (ent = getmntent(aFile))) {
    tbl[ent->mnt_fsname] = ent->mnt_dir;
  }
  endmntent(aFile);
}

void get_partitions (const std::string& dev, std::vector<std::tuple<std::string, std::string, std::string>>& partitions)
{
  blkid_probe pr = blkid_new_probe_from_filename(dev.c_str());
  if (!pr) {
    std::stringstream iss;
    iss << "Failed to open " << dev << " (maybe trying running as root)";
    throw std::runtime_error(iss.str());
    return;
  }
  // Get number of partitions
  blkid_partlist ls;
  int nparts;

  ls = blkid_probe_get_partitions(pr);
  nparts = blkid_partlist_numof_partitions(ls);
  partitions.resize(nparts);

  if (nparts <= 0){
    throw std::runtime_error("Please enter correct device name! e.g. \"/dev/sdc\"");
    return;
  }

  // Get UUID, label and type
  const char *label;
  const char *type;

  for (int idx = 0; idx < nparts; idx++) {

    std::get<0>(partitions[idx]) = dev + std::to_string(idx + 1);
    pr = blkid_new_probe_from_filename(std::get<0>(partitions[idx]).c_str());
    blkid_do_probe(pr);
    blkid_probe_lookup_value(pr, "LABEL", &label, NULL);
    std::get<1>(partitions[idx]) = label;

    blkid_probe_lookup_value(pr, "TYPE", &type, NULL);
    std::get<2>(partitions[idx]) = type;

  }

  blkid_free_probe(pr);
}

void create_dir_if_no_exist (const std::string& path) {
  struct stat sb;
  if (!(stat(path.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode))) {
    if (mkdir(path.c_str(), 0766)) {
      std::stringstream iss;
      iss << "mkdir error: " <<  strerror(errno);
      throw std::runtime_error(iss.str());
    }
  }
}

bool file_exist (const std::string& filename) {
  struct stat buffer;
  return (stat (filename.c_str(), &buffer) == 0);
}



int main (int argc, char** argv) {
  if (argc > 1) {
    #if DEBUG
    std::ostream& out = std::cerr;
    #else
    std::ofstream logfile;
    logfile.open(log_file_path, std::ios_base::app);
    std::ostream& out = logfile;
    #endif

    std::string dev = argv[1];

    std::map<std::string, std::string> mnt_tbl;
    std::vector<std::tuple<std::string, std::string, std::string>> partitions;
    get_mnt_tbl(mnt_tbl);
    get_partitions(dev, partitions);
    auto dev_part_info = partitions[0];
    std::string dev_part = std::get<0>(dev_part_info);

    std::string mnt_dir;
    bool mnt_existing;

    if (mnt_tbl.find(dev_part) != mnt_tbl.end()) {
      mnt_dir = mnt_tbl[dev_part];
      mnt_existing = true;
    } else {
      mnt_existing = false;
      mnt_dir = "/media/" + std::get<1>(dev_part_info);
      create_dir_if_no_exist(mnt_dir);
      out << "Mounting " << dev_part << " at " << mnt_dir << std::endl;

      if (mount(dev_part.c_str(), mnt_dir.c_str(), "vfat", MS_NOATIME, NULL)) {
        if (errno == EBUSY) {
          throw std::runtime_error("Mountpoint busy");
        } else {
          std::stringstream iss;
          iss << "Mount error: " <<  strerror(errno);
          throw std::runtime_error(iss.str());
        }
      } else {
        out << "Mount successful" << std::endl;;
      }
    }

    // now get those wifi details!

    std::string wifi_txt_path = mnt_dir + "/wifi.txt";
    if (file_exist(wifi_txt_path)) {
      std::fstream wifi_file;
      wifi_file.open(wifi_txt_path, std::ios::in);
      std::string ssid;
      std::string password;
      if (wifi_file.is_open()){   //checking whether the file is open
        getline(wifi_file, ssid);
        getline(wifi_file, password);
        wifi_file.close(); //close the file object.
      } else {
        std::stringstream iss;
        iss << wifi_txt_path << " not open!";
        throw std::runtime_error(iss.str());
      }

      out << "Connecting to wifi network" << std::endl;
      std::stringstream nmcli_cmd;
      nmcli_cmd << "nmcli device wifi connect " << ssid << " password " << password;
      if (system(nmcli_cmd.str().c_str())) {
        std::stringstream iss;
        iss << "nmcli error: " <<  strerror(errno);
        throw std::runtime_error(iss.str());
      } else {
        out << "Connection successful" << std::endl;
      }

    } else {
      std::stringstream iss;
      iss << wifi_txt_path << " doesn't exist";
      throw std::runtime_error(iss.str());
    }



    if (! mnt_existing) {
      // umount directory
      if (umount(mnt_dir.c_str())) {
        std::stringstream iss;
        iss << "Umount error: " <<  strerror(errno);
        throw std::runtime_error(iss.str());
      } else {
        out << "Umount successful" << std::endl;
      }
      // remove created mount directory
      if (rmdir(mnt_dir.c_str())) {
        std::stringstream iss;
        iss << "rmdir error: " <<  strerror(errno);
        throw std::runtime_error(iss.str());
      }
    }


    // logfile << "wifi-detect: dev=" << dev << ", mnt_dir=" << mnt_dir << std::endl;
    // out << "wifi-detect: dev=" << dev << ", mnt_dir=" << mnt_dir << std::endl;
    #if DEBUG == 0
    logfile.close();
    #endif
  } else {
    throw std::runtime_error("Need to pass a single argument");
    return 0;
  }
}
